module.exports = function (grunt) {
	grunt.initConfig({    
		pkg: grunt.file.readJSON('package.json'),

		/*
		 * The JS Hint task
		 */
		jshint: {
		  all: ['jquery.sticky.js']
		},

		/*
		 * The Uglify task
		 */
		uglify: {
		  options: {
			banner: '/*! <%= pkg.title %> <%= pkg.version %> <%=grunt.template.today("yyyy-mm-dd")%> */'
		  },
		  dist: {
			src: [
			  'jquery.sticky.js'
			],
			dest: 'jquery.sticky.min.js'
		  },
		},

		/*
		 * The Watch task (default)
		 */
		watch: {
		  scripts: {
			files: [
			  'jquery.sticky.js'
			],
			tasks: ['jshint', 'uglify']
		  }
		}
	});
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	//register default task
	grunt.registerTask('default', 'watch');

	//register a custom build task
	grunt.registerTask('build', ['jshint', 'uglify']);
}